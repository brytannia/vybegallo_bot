require 'telegram/bot'
require_relative 'vybegallo_bot/perisher'
require_relative 'vybegallo_bot/babbler'
token = ENV['MY_TOKEN']

Telegram::Bot::Client.run(token) do |bot|
  bot.listen do |message|
    if message.reply_to_message.nil?
      case message.text
      when '/погибаю'
        text = Perisher.new.call
        bot.api.sendMessage(chat_id: message.chat.id, text: text)
      when '/погибаю_без_эликсира'
        bot.api.sendMessage(chat_id: message.chat.id, text: 'Монады')
        bot.api.sendMessage(chat_id: message.chat.id, text: 'Мо на ды')
        bot.api.sendMessage(chat_id: message.chat.id, text: 'Монаааааады')
      else
        text = message.from.username == 'brytannia' ? 'Трудно быть ботом, но я стараюсь!' : Babbler.new.call
        bot.api.sendMessage(chat_id: message.chat.id, text: text)
      end
    end
  end
end